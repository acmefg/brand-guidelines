<html>
<head>
    <meta charset="utf-8">
    <title>The Acme Facilities Group Brand Guidelines</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="//fonts.googleapis.com/css?family=Patua+One" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet">
    <link media="screen, projection, print" rel="stylesheet" href="asset/common.bundle.css?<?php echo filemtime('asset/common.bundle.css') ?>">
</head>
<body>
    <div class="site-container">
        <div class="logo-container">
            <a href="#" class="logo-acmefg"><?php include 'logo.svg' ?></a>
        </div>
        <div class="panel-header-container">
            <h1 class="panel-header">Form</h1>
        </div>
        <div class="panel-container">
            <form action="">
                <div class="form-group">
                    <label for="form-example" class="form-label block">Input Label</label>
                    <input id="form-example" class="form-input" type="text" value="Hello World">
                </div>
                <div class="form-group">
                    <input class="form-input" type="text" autocomplete="Hello World">
                </div>
                <div class="form-group">
                    <textarea class="form-textarea" cols="30" rows="10">Textarea</textarea>
                </div>
                <select class="form-select">
                    <option value="">Choose</option>
                </select>
                <div class="">
                    <button class="button">Submit</button>
                </div>
            </form>
        </div>
        <div class="panel-header-container">
            <h1 class="panel-header">Heading</h1>
        </div>
        <div class="panel-container">
            <div class="panel-demo typography">

<?php for ($index = 1; $index < 7; $index++) { ?>

                <h<?php echo $index ?>>Heading <?php echo $index ?></h<?php echo $index ?>>

<?php }  ?>

            </div>
        </div>
        <div class="panel-header-container">
            <h1 class="panel-header">Message</h1>
        </div>
        <div class="panel-container">
            <h2 class="panel-header-2">Normal</h2>
            <div class="alert">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus lorem ante, vitae ornare dolor pellentesque nec. Phasellus non nibh ut urna pulvinar commodo.</p>
            </div>
            <h2 class="panel-header-2">Success</h2>
            <div class="alert success">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus lorem ante, vitae ornare dolor pellentesque nec. Phasellus non nibh ut urna pulvinar commodo.</p>
            </div>
            <h2 class="panel-header-2">Fail</h2>
            <div class="alert fail">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus lorem ante, vitae ornare dolor pellentesque nec. Phasellus non nibh ut urna pulvinar commodo.</p>
            </div>
        </div>
        <div class="panel-header-container">
            <h1 class="panel-header">Button</h1>
        </div>
        <div class="panel-container">
            <button class="button m1">Button Primary</button>
            <button class="button secondary m1">Button Secondary</button>
            <button class="button disabled m1">Button Disabled</button>
        </div>
        <div class="panel-header-container">
            <h1 class="panel-header">Color</h1>
        </div>
        <div class="panel-container clearfix">
            <span class="color-way background-color-0"></span>
            <span class="color-way background-color-1-0"></span>
            <span class="color-way background-color-2-0"></span>
            <span class="color-way background-color-3-0"></span>
        </div>
        <div class="panel-header-container">
            <h1 class="panel-header">Table</h1>
        </div>
        <div class="panel-container clearfix">
            <table class="table">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Telephone</th>
                    <th>Mobile</th>
                    <th>Age</th>
                </tr>
                <tr>
                    <td>Martin</td>
                    <td>Wyatt</td>
                    <td>1283908131890</td>
                    <td>1283129038129</td>
                    <td>45</td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
